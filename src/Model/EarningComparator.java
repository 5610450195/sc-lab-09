package Model;

import java.util.Comparator;

public class EarningComparator implements Comparator<Company>{

	@Override
	public int compare(Company com1, Company com2) {
		if (com1.getIncome() < com2.getIncome()) {
			return -1;
		}
		else if (com1.getIncome() > com2.getIncome()) {
			return 1;
		}
		else return 0;
	}
}
