package Model;

import java.util.Comparator;

import Interface.Taxable;

public class taxComparator implements Comparator<Taxable>{

	@Override
	public int compare(Taxable com1, Taxable com2) {
		if (com1.getTax() < com2.getTax()) {
			return -1 ;
		}
		else if (com1.getTax() > com2.getTax()) {
			return 1 ;
		}
		else return 0 ;
	}
}
