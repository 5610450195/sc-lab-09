package Model;

import Interface.Taxable;

public class Person implements Taxable, Comparable {

	private String name;
	private double yearlyIncome;

	public Person(String name, double yearlyIncome) {
		this.name = name;
		this.yearlyIncome = yearlyIncome;
	}

	public String getName() {
		return this.name;
	}

	public double getYearlyIncome() {
		return this.yearlyIncome;
	}

	@Override
	public double getTax() {
		double income = this.getYearlyIncome();
		double result = 0;

		if (income > 300000) {
			result += 0.05 * 300000;
			income -= 300000;
			result += 0.10 * income;
		}else{
			result += 0.05 * income;
		}
		return result;
	}

	@Override
	public int compareTo(Object otherObject) {
		Person other = (Person)otherObject;
		if (yearlyIncome < other.yearlyIncome) {
			return -1;
		}
		else if (yearlyIncome > other.yearlyIncome) {
			return 1;
		}
		else return 0;
	}
	
	public String toString() {
		return name+" >> "+yearlyIncome;
	}

}
