package Model;
import java.util.Comparator;


public class ExpenseComparator implements Comparator<Company>{

	@Override
	public int compare(Company com1, Company com2) {
		if (com1.getExpenses() < com2.getExpenses()) {
			return -1;
		}
		else if (com1.getExpenses() > com2.getExpenses()) {
			return 1;
		}
		else return 0;
	}


}
