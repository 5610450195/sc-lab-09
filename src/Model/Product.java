package Model;

import Interface.Taxable;

public class Product implements Taxable, Comparable {
	private String name;
	private double price;

	public Product(String name, double price) {
		this.name = name;
		this.price = price;
	}

	public String getName() {
		return this.name;
	}

	public double getPrice() {
		return this.price;
	}

	@Override
	public double getTax() {
		return 0.07 * this.getPrice();
	}

	@Override
	public int compareTo(Object otherObject) {
		Product other = (Product)otherObject;
		if (price < other.price) {
			return -1;
		}
		else if (price > other.price) {
			return 1;
		}
		else return 0;
	}
	
	public String toString() {
		return name+" >> "+price;
	}
}
