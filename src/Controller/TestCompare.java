package Controller;


import java.util.ArrayList;
import java.util.Collections;

import Interface.Taxable;
import Model.Company;
import Model.EarningComparator;
import Model.ExpenseComparator;
import Model.Person;
import Model.Product;
import Model.ProfitComparator;
import Model.taxComparator;

public class TestCompare {
	public static void main(String[] args) {
		TestCompare tester = new TestCompare();
		tester.testPerson();
		tester.testProduct();
		tester.testCompany();
	}
	
	public void testPerson() {
		ArrayList<Comparable> person = new ArrayList<Comparable>();
		person.add (new Person("First", 1200000));
		person.add (new Person("Second", 1000000));
		person.add (new Person("Third", 1500000));
		System.out.println("------Person------");
		for (Comparable ob : person) {
			System.out.println(ob);
		}
		Collections.sort(person);
		System.out.println("--After sorted--");
		for (Comparable ob : person) {
			System.out.println(ob);
		}
	}
	
	public void testProduct() {
		ArrayList<Comparable> product = new ArrayList<Comparable>();
		product.add (new Product("First", 120));
		product.add (new Product("Second", 100));
		product.add (new Product("Third", 150));
		System.out.println("------Product------");
		for (Comparable ob : product) {
			System.out.println(ob);
		}
		Collections.sort(product);
		System.out.println("--After sorted--");
		for (Comparable<String> ob : product) {
			System.out.println(ob);
		}
	}
	
	public void testCompany() {
		ArrayList<Company> company = new ArrayList<Company>();
		company.add(new Company("First", 3000 , 350));
		company.add(new Company("Second", 5000 , 220));
		company.add(new Company("Third", 4000 , 400));
		System.out.println("------Company------");
		for (Company ob : company) {
			System.out.println(ob);
		}
		Collections.sort(company, new EarningComparator());
		System.out.println("--After sorted earning--");
		for (Company ob : company) {
			System.out.println(ob);
		}
		Collections.sort(company, new ExpenseComparator());
		System.out.println("--After sorted expense--");
		for (Company ob : company) {
			System.out.println(ob);
		}
		Collections.sort(company, new ProfitComparator());
		System.out.println("--After sorted profit--");
		for (Company ob : company) {
			System.out.println(ob);
		}
		Collections.sort(company, new taxComparator());
		System.out.println("--After sorted Tax--");
		for (Company ob : company) {
			System.out.println(ob);
		}
		
		ArrayList<Taxable> allTax = new ArrayList<Taxable>();
		allTax.add(new Person("First", 10000));
		allTax.add(new Person("Second", 30000));
		allTax.add(new Company("First", 20000 , 1000));
		allTax.add(new Company("Second", 10000 , 500));
		allTax.add(new Product("First", 10000));
		allTax.add(new Product("Second", 100));
		Collections.sort(allTax, new taxComparator());
		System.out.println("------After sorted all tax------\n");
		for (Taxable ob : allTax) {
			System.out.println(ob);
		}
	}
}
