package Controller;

import java.util.ArrayList;
import Interface.Traversal;
import TreeTraversal.InOrderTraversal;
import TreeTraversal.Node;
import TreeTraversal.PostOrderTraversal;
import TreeTraversal.PreOrderTraversal;
import TreeTraversal.ReportConsole;

public class TraverseTest {
	
	public static void main(String[] args) {
		Node a = new Node("A",null,null);
		Node c = new Node("C",null,null);
		Node e = new Node("E",null,null);
		Node d = new Node("D",c,e);
		Node b = new Node("B",a,d);
		Node h = new Node("H",null,null);
		Node i = new Node("I",null,null);
		Node g = new Node("G",null,i);
		Node f = new Node("F",b,g);
		
		ReportConsole RC = new ReportConsole();
		System.out.print("Traverse with PreOrderTraversal:	");
		RC.display(f,new PreOrderTraversal());
		System.out.print("\nTraverse with InOrderTraversal:		");
		RC.display(f,new InOrderTraversal());
		System.out.print("\nTraverse with PostOrderTraversal:	");
		RC.display(f,new PostOrderTraversal());
	}
}
