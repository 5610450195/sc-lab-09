package TreeTraversal;


import java.util.ArrayList;
import java.util.List;

import Interface.Traversal;

public class PostOrderTraversal implements Traversal{

	@Override
	public List<Node> traverse(Node node) {
		List<Node> n = new ArrayList<Node>();
		if (node == null) {
			return n;
		}
		if (node.getLeft() != null) {
			n.addAll(traverse(node.getLeft()));
		}
		if (node.getRight() != null) {
			n.addAll(traverse(node.getRight()));
		}
		n.add(node);
		return n;
	}




}
