package TreeTraversal;

import java.util.ArrayList;
import java.util.List;

import Interface.Traversal;

public class InOrderTraversal implements Traversal{

	@Override
	public List<Node> traverse(Node node) {
		List<Node> n = new ArrayList<Node>();
		if (node == null) {
			return n;
		}
		if (node.getLeft() != null) {
			n.addAll(traverse(node.getLeft()));
		}
		n.add(node);
		if (node.getRight() != null) {
			n.addAll(traverse(node.getRight()));
		}
		return n;
	}


}
