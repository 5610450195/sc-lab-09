package TreeTraversal;

import java.util.List;

import Interface.Traversal;

public class ReportConsole {
	public static void display (Node root, Traversal object){
		List<Node> node = object.traverse(root);
		for (Node n: node) {
			System.out.print(n.getValue()+" ");
		}
	}
}
