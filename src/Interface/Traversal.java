package Interface;

import java.util.List;

import TreeTraversal.Node;

public interface Traversal {
	List<Node> traverse(Node node);
}
